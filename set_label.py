from pymongo import MongoClient
import re
import os.path

#label_file = sys.argv[1]
#collection_name = sys.argv[2]


#we copy the func.idx file and label the record based on it. The last column is the label value
#0 -- error path; 1 -- correct path
def set_prelabel(label_file, collection_name):
	if not os.path.isfile(label_file):
		print 'skip set labelling'
		return

	client = MongoClient()
	db = client.errspec
	table = db[collection_name]

	with open(label_file) as f:
		for line in f:
			m = re.search(r'(.*?);(.*);(.*)', line)
			sig = m.group(1)
			label = m.group(3)
			if int(label) == 0:
				rlabel = 'Error'
			else:
				rlabel = 'Correct'

			#print sig
			res = table.update_one({'sig': sig}, {'$set': {'Prelabel': rlabel}})
			#print res.matched_count, res.modified_count
