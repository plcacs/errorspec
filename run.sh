#!/bin/bash

declare -A runOrder

runOrder['zlib']='./zlib'
runOrder['httpd']='./httpd-2.4.29'
runOrder['wget']='./wget-1.19'
runOrder['glibc']='./glibc'
runOrder['gtk']='./gtk+'
runOrder['kernel_fs']='./linux'
runOrder['libgcrypt']='./libgcrypt-1.8.1'
runOrder['vlc']='./vlc-2.1.4'
runOrder['gnutls']='./gnutls-3.5.0'
runOrder['ssl']='./openssl'

if [ ! $# -eq 2 ]; then
	echo "input the DB name(\$1) and the output dir(\$2)"
	exit 1
fi

#for key in ${!runOrder[@]}
#do
#	echo ${key}';'${runOrder[${key}]} 
#done

if [ -d $2 ]; then
	echo $2 "already exists"
	exit 1
fi

mkdir $2

cat project_lst | while read line
do
	echo $line
	proj=`echo $line | cut -d ';' -f 1`
	dirn=`echo $line | cut -d ';' -f 2`
	>log.$proj
	echo '!!!!!!!!!!!!!!! '$proj' !!!!!!!!!!!!!!!!!' >> log.$proj
	python run.py $1 $proj $dirn >> log.$proj

	cp log.$proj $2
	cp $proj\_1.cres $proj\_2.cres $2
done

cp project_lst $2

echo "All results are stored into dir:" $2
