import sys
import re
from pymongo import MongoClient
import ast
from operator import itemgetter
from collections import Counter
import expressions.compiler as Exp
import jaccard
import timeit

from func_info_extract import MACRO_TYPE, CONSTANT_TYPE, VARIABLE_TYPE, FUNC_TYPE, OPERATION_TYPE 

def checkSig(func_file, sig):
	with open(func_file) as f:
		for line in f:
			if line.strip('\n') in sig:
				return True
	
	return False


def DL_dist(l1, l2):
	d = {}
	len1 = len(l1)
	len2 = len(l2)

	for i in xrange(-1, len1+1):
		d[(i, -1)] = i + 1
	for j in xrange(-1, len2+1):
		d[(-1, j)] = j + 1

	for i in xrange(len1):
		for j in xrange(len2):
			if l1[i] == l2[j]:
				cost = 0
			else:
				cost = 1.5

			d[(i, j)] = min(
					d[(i-1, j)] + 0.5, #delete
					d[(i, j-1)] + 0.5, #insert
					d[(i-1, j-1)] + cost #substitue
					)

			if i and j and l1[i] == l2[j-1] and l1[i-1] == l2[j]:
				d[(i, j)] = min(d[(i, j)], d[(i-2, j-2)] + cost) #transpose

	return d[(len1-1, len2-1)]

def find_max_match_bracket(s):
	in_cnt = 0
	out_cnt = 0
	in_pos = -1
	out_pos = -1

	for pos, c in enumerate(s):
		if c == '[':
			in_cnt += 1
			if in_pos == -1:
				in_pos = pos

		if c == ']':
			out_cnt += 1
			out_pos = pos

			if out_cnt - in_cnt == 0:
				return (in_pos, out_pos)

	return (-1, -1)

#shit, the following two functions are urgly......
def filter_specialChar(s):
	#check if it has fixed string
	m = re.search(r'["\']([A-Za-z0-9\~\!\@\#\$\^\&\*\(\)\+\=\{\}\[\]\:\;\"\'\<\>\,\?\|\%_\./\\-].*?)["\']', s)
	if m:
		return s[:m.start()] + 'STR' + s[m.end():]

	#check if it has a[]
	start, end = find_max_match_bracket(s)
	if start != -1:
		return s[:start] + s[end+1:]

	#check if it has (type *)a
	m = re.search(r'\(\w+\**\)(?=\w+|\()', s)
	if m:
		return s[:m.start()] + s[m.end():]

	#check if we have sizeof(type *)
	m = re.search(r'\*\)', s)
	if m:
		return s[:m.start()] + s[m.end() - 1:]

	#check if it has (& a)
	m = re.search(r'&&', s)
	if m:
		s1 = s[:m.start()]
		s2 = s[m.end():]

		return filter_specialChar(s1) + '&&' + filter_specialChar(s2)
	else:
		m = re.search(r'(?<=[()]|,)&', s)
		if m:
			return s[:m.start()] + s[m.end():]

	return s

def filter_wrapper(s):
	s = s.replace(' ', '')

	while(1):
		res = filter_specialChar(s)
		if res == s:
			return res
		else:
			s = res

#copy from func_info_extract.py, but just get the function on the rhs
def get_func_name(code):
	code = code.replace(' ', '')
	m = re.search(r'([\w0-9]+)\(.*\)', code)

	if m:
		left = re.search(r'=', code)
		if left:
			return get_func_name(code[left.start() + 1:])
		else:
			return m.group(1)
	else:
		return None


def parse_line(line):
	m = re.search(r'(.*);(.*);(.*);(.*)~(.*)', line)
	return (m.group(1), ast.literal_eval(m.group(2)), int(m.group(5)))

class CorrectProcess():
	def __init__(self, resF, funcF, coll_n):
	    self.res_file = resF
	    #self.idx_file = idxF
	    self.func_file = funcF

	    self._client = MongoClient()
	    self._db = self._client.errspec
	    self.feature_table = self._db[coll_n]

	def _get_Cresult(self, sl, el):
		res = []
		with open(self.res_file) as f:
			for lid, line in enumerate(f):
				if lid < sl:
					continue
				elif lid > el:
					break
				else:
					#m = re.search(r': (.*)', line)
					#c = int(m.group(1))
					res.append(int(line))

		return res

	#first round result, set everything except classification result to be the default value
	def _db_update(self, sigLst, cres):
		for i, c in enumerate(cres):
			index = sigLst[i]
			self.feature_table.update_one({'sig': index}, {'$set': {'ClassificationRes': c, 'Determined': 0, 'mConflict': 0, 'fConflict': 0}})

	def _db_self_update(self, sigLst, cres):
		for i, c in enumerate(cres):
			index = sigLst[i]
                        self.feature_table.update_one({'sig': index}, {'$set': {'ClassificationRes': c, 'Determined': 1, 'mConflict': 0, 'fConflict': 0, 'Prelabel': 'Self'}})

	def _db_BatchUpdate(self, sigLst, retLst, cres):
		chkd_lst = []
		for n, v in enumerate(sigLst):
			if n in chkd_lst:
				continue
			else:
			#sometimes the function can be defined multiple times in one file by using #ifdef
				key_indices = [i for i, x in enumerate(retLst) if x == retLst[n] and i < len(cres)]
				values = itemgetter(*key_indices)(cres)
				if type(values) == int:
					mconflict = 0
				else:
					values = list(values) 
					if len(list(set(values))) == 1:
						mconflict = 0
					else:
						mconflict = 1
							
				#update DB in batch
				#print "####UPDATE DB#####"
				for k in key_indices:
					index = sigLst[k]
		#if fconflict == 1, it means that all paths in the function are classified into one class
		#if mconflict == 1, it means that paths with the same returned variable are put into different classes
					if len(list(set(cres))) == 1:
						fconflict = 1
					else:
						fconflict = 0

					#print k, retLst[k], mconflict, cres[k], fconflict
					self.feature_table.update_one({'sig': index}, {'$set': {'ClassificationRes': cres[k], 'fConflict': fconflict, 'mConflict': mconflict, 'Determined': 1}})

				chkd_lst.extend(key_indices)

	def store_classification_res(self, lr):
		with open(self.res_file) as f:
			fn = ''
			retLst = []
			sigLst = []
                        cres = []
			for lid, line in enumerate(f):
				sig, retVar, c = parse_line(line)

				m = re.search(r'(.*)@(.*):(.*):(.*)', sig)
				func_name = m.group(1)

				if fn == '':
					fn = func_name
					retLst.append(str(retVar[0]).replace(' ',''))
					sigLst.append(sig)
                                        cres.append(c)

				elif fn == func_name:
					retLst.append(str(retVar[0]).replace(' ',''))
					sigLst.append(sig)
                                        cres.append(c)

				else:
					#OK, we have a new func here...see if we need to store the classification res of the old func first
					#print "====================="
					#print fn, retLst
					#sline = lid - len(sigLst) 
					#eline = lid - 1
					#cres = self._get_Cresult(sline, eline)
					
					#print sigLst, len(sigLst)
					#print retLst, len(retLst)
					#print cres, len(cres)
					if lr == 1:
						self._db_update(sigLst, cres)
                                        elif lr == 3:
                                                self._db_self_update(sigLst, cres)
					else:
						self._db_BatchUpdate(sigLst, retLst, cres)


					retLst[:] = []
					sigLst[:] = []
                                        cres[:] = []
					fn = func_name
					retLst.append(str(retVar[0]).replace(' ',''))
					sigLst.append(sig)
                                        cres.append(c)

			#remember to handle the last funcs here
			#print fn, retLst
			#sline = lid - len(sigLst) + 1
			#eline = lid
			#cres = self._get_Cresult(sline, eline)
			if lr == 1:
				self._db_update(sigLst, cres)
                        elif lr == 3:
                                self._db_self_update(sigLst, cres)
			else:
				self._db_BatchUpdate(sigLst, retLst, cres)



	def set_pathLabel(self):
		for i in self.feature_table.find({'Determined': 1}):
			if i['ClassificationRes'] == self.error_class:
				self.feature_table.update_one({'sig': i['sig']}, {'$set': {'Prelabel': 'Error'}})
			elif i['ClassificationRes'] == self.correct_class:
				self.feature_table.update_one({'sig': i['sig']}, {'$set': {'Prelabel': 'Correct'}})

	def _get_label_class(self, label):
		res = []
		for i in self.feature_table.find({'Prelabel': label}):
			c = i['ClassificationRes']
			if c != -1:
				res.append(c)

		return Counter(res).most_common()


	def _propagate_label(self, class_id):
		for i in self.feature_table.find({'ClassificationRes': class_id}):
			self.feature_table.update_one({'sig': i['sig']}, {'$set': {'Determined': 1}})

			#if i['Determined'] == 1:
				#if the record we label has conflict, remove the conflict directly
				#if i['Prelabel'] != 'NaN' and (i['fConflict'] == 1 or i['mConflict'] == 1):
					#self.feature_table.update_one({'sig': i['sig']}, {'$set': {'fConflict': 0, 'mConflict': 0}})
			#else:
				#if there is no conflict for the record, we mark it as 'determined'
				#if i['fConflict'] == 0 and i['mConflict'] == 0:
					#self.feature_table.update_one({'sig': i['sig']}, {'$set': {'Determined': 1}})
	

	def _match_retVar(self, doc, var_lst):
		ret_var = doc['returnVar']
		ret_type = doc['returnType'][0]
		ret_val = doc['Retrelationtype']

		if ret_type == VARIABLE_TYPE or (ret_type == CONSTANT_TYPE and ret_val == 0):
			#match locally
			m = re.search(r'(.*):', doc['sig'])
			fsig = m.group(1)
			res = self.feature_table.find({'sig': {'$regex': fsig}, 'returnVar': ret_var})
			#print '---local match---', res.count()
		else:
			#match globally
			res = self.feature_table.find({'returnVar': ret_var})
			#print '---global match---', res.count()
			var_lst.append(ret_var)

		return res

	def _update_PrelabelClassification(self, label, class_id):
		var_lst = []
		cnt = 0

		for i in self.feature_table.find({'Prelabel': label}):
			#print '############:', i['sig'], i['returnVar']
			ret = i['returnVar']
			if ret in var_lst:
				continue
			
			for j in self._match_retVar(i, var_lst):
				if j['SingleRet'] == 1:
					continue

				if j['ClassificationRes'] != class_id:
					#print j['returnVar'], j['ClassificationRes'], class_id
					self.feature_table.update_one({'sig': j['sig']}, {'$set': {'ClassificationRes': class_id, 'Determined': 1}})
					if checkSig(self.func_file, j['sig']) == True:
						cnt += 1

		return cnt

	
	def _update_DetermineClassification(self, class_id):
		var_lst = []
		cnt = 0

		for i in self.feature_table.find({'ClassificationRes': class_id}):
			ret = i['returnVar']
			if ret in var_lst:
				continue

			for j in self._match_retVar(i, var_lst):
				if j['SingleRet'] == 1:
					continue

				if j['ClassificationRes'] != class_id:
					self.feature_table.update_one({'sig': j['sig']}, {'$set': {'ClassificationRes': class_id, 'Determined': 1}})
					if checkSig(self.func_file, j['sig']) == True:
						cnt += 1

		return cnt

	
	def label_propagate(self, flag=1):
	#determine which class is for the error path and which class is for the correct path based on the given label values
	#propagate the label values to the corresponding records and mark them as 'determined'
		propagate_cnt = 0

		tc = self._get_label_class('Correct')
		if not tc:
			print 'can not determine the nonerror class'

		fc = self._get_label_class('Error')
		if not fc:
			print 'can not determine the error class'
		
		if not tc or not fc:
			print 'try to provide more label data...'
			sys.exit(1)

		self.correct_class, freq = tc[0]
		self.error_class, freq = fc[0]

		if self.error_class == self.correct_class:
			print self.error_class
			print 'the error class and nonerror class are the same'
			print 'try to provide more label data...'
			sys.exit(1)

		print "Correct class id:", self.correct_class
		print "Error class id:", self.error_class

		self._propagate_label(self.correct_class)
		self._propagate_label(self.error_class)

		if flag == 0:
			print "without label propagation..."
			return

		propagate_cnt += self._update_PrelabelClassification('Correct', self.correct_class)
		propagate_cnt += self._update_PrelabelClassification('Error', self.error_class)

		propagate_cnt += self._update_DetermineClassification(self.correct_class)
		propagate_cnt += self._update_DetermineClassification(self.error_class)

		print "Propagate count:", propagate_cnt


	def _get_class_retVar(self, class_id):
		res = []
		for i in self.feature_table.find({'ClassificationRes': class_id, 'Determined': 1}):
			ret = i['returnVar'][0]
			res.append(ret.replace(' ', ''))

		return res


	def _retVar_voting(self, docs, ctype, first=True):
		ret_lst = []
		sig_lst = []
		threshold_err = 0.6
		threshold_correct = 0.4

		for i in docs:
			if i['Determined'] == 1:
				continue

			if first and i['ClassificationRes'] != self.error_class and i['ClassificationRes'] != self.correct_class:
				continue

			ret = i['returnVar'][0]
			ret_lst.append(ret.replace(' ', ''))
			sig_lst.append(i['sig'])

		if not ret_lst:
			return

		tmp = Counter(ret_lst).most_common()
		err_ret, freq = tmp[0]

		ratio = float(freq) / docs.count()
		#we only vote for one siginificant error path
		#we do not update the global errRet list since (1) we are in correcting process. Not 100% sure if we are doing this correctly (2) we may have conflict when update the errRet list
		if ratio > threshold_err:
			for n, i in enumerate(ret_lst):
				if i == err_ret:
					self.feature_table.update_one({'sig': sig_lst[n]}, {'$set': {'fConflict': 0, 'mConflict': 0, 'ClassificationRes': self.error_class, 'Determined': 1}})
				else:
					if ctype == 'fConflict':
						self.feature_table.update_one({'sig': sig_lst[n]}, {'$set': {'fConflict': 0, 'mConflict': 0, 'ClassificationRes': self.correct_class, 'Determined': 1}})

		correct_ret, freq = tmp[-1]
		ratio = float(freq) / docs.count()
		#we only vote for one significant correct path
		if ratio < threshold_correct:
			for n, i in enumerate(ret_lst):
				if i == correct_ret:
					self.feature_table.update_one({'sig': sig_lst[n]}, {'$set': {'fConflict': 0, 'mConflict': 0, 'ClassificationRes': self.correct_class, 'Determined': 1}})
				else:
					if ctype == 'fConflict':
						self.feature_table.update_one({'sig': sig_lst[n]}, {'$set': {'fConflict': 0, 'mConflict': 0, 'ClassificationRes': self.error_class, 'Determined': 1}})



	def _retVar_globalChk(self, docs, first=True):
		errD = dict()
		for i in self.global_errRet:
			errD.update({i[0]:i[1]})
		
		correctD = dict()
		for i in self.global_correctRet:
			correctD.update({i[0]:i[1]})
		
		for i in docs:
			if i['Determined'] == 1:
				continue
			
			if first and i['ClassificationRes'] != self.error_class and i['ClassificationRes'] != self.correct_class:
				continue

			ret = i['returnVar'][0].replace(' ', '')
			#it might be possible that the same ret exists in both errD and correctD
			if ret in errD and ret not in correctD:
				class_id = self.error_class
			elif ret in correctD and ret not in errD:
				class_id = self.correct_class
			else:
				class_id = None

			if class_id:
				self.feature_table.update_one({'sig': i['sig']}, {'$set': {'fConflict': 0, 'mConflict': 0, 'ClassificationRes': class_id, 'Determined': 1}})


	def _retVar_determined(self, docs):
		#get the determined ret list first
		errVar_lst = []
		correctVar_lst = []

		for i in docs:
			if i['Determined'] == 1:
				ret = i['returnVar'][0].replace(' ', '')

				if i['ClassificationRes'] == self.error_class:
					errVar_lst.append(ret)
				else:
					correctVar_lst.append(ret)

		docs.rewind()
		for i in docs:
			if i['fConflict'] == 1 or i['mConflict'] == 1:
				ret = i['returnVar'][0].replace(' ', '')

				if ret in errVar_lst and ret not in correctVar_lst:
					self.feature_table.update_one({'sig': i['sig']}, {'$set': {'fConflict': 0, 'mConflict': 0, 'ClassificationRes': self.error_class, 'Determined': 1}})
				
				if ret in correctVar_lst and ret not in errVar_lst:
					self.feature_table.update_one({'sig': i['sig']}, {'$set': {'fConflict': 0, 'mConflict': 0, 'ClassificationRes': self.correct_class, 'Determined': 1}})


	def _vote_correct(self, cres):
		class_lst = [i['ClassificationRes'] for i in cres]
		sig_lst = [i['sig'] for i in cres]
		class_id, freq = Counter(class_lst).most_common(1)[0]

		if float(freq) / len(class_lst) > 0.7:
			print 'use voting'
			for idx, sig in enumerate(sig_lst):
				if class_lst[idx] != class_id:
					print "resolve conflict:", sig, i['returnVar'], class_id
				self.feature_table.update_one({'sig': sig}, {'$set': {'ClassificationRes': class_id, 'mConflict': 0, 'fConflict': 0}})

			return True

		return False


	def _cross_check(self, i, func_pattern):
		ret_type = i['returnType'][0]
		ret_value = i['Retrelationtype']
		x = (ret_type, ret_value)
		for j in func_pattern:
			if x in j:
				class_id = j[x]
				if i['ClassificationRes'] != class_id:
					print "resolve conflict:", i['sig'], i['returnVar'], class_id
				self.feature_table.update_one({'sig': i['sig']}, {'$set': {'ClassificationRes': class_id}})
				break


	def _heuristic_correct(self, i):
		if i['Retrelationtype'] == 2:
			class_id = self.error_class
			sig = i['sig']
			if i['ClassificationRes'] != class_id:
				print "resolve conflict:", i['sig'], i['returnVar'], class_id
			self.feature_table.update_one({'sig': sig}, {'$set': {'ClassificationRes': class_id}})
		elif i['Retrelationtype'] == 3:
			class_id = self.correct_class
			sig = i['sig']
			if i['ClassificationRes'] != class_id:
				print "resolve conflict:", i['sig'], i['returnVar'], class_id
			self.feature_table.update_one({'sig': sig}, {'$set': {'ClassificationRes': class_id}})
		elif i['Retrelationtype'] == 0 and i['returnType'][0] == MACRO_TYPE:
			class_id = self.correct_class
			sig = i['sig']
			if i['ClassificationRes'] != class_id:
				print "resolve conflict:", i['sig'], i['returnVar'], class_id
			self.feature_table.update_one({'sig': sig}, {'$set': {'ClassificationRes': class_id}})
		elif i['Retrelationtype'] == 1 and i['returnType'][0] == CONSTANT_TYPE:
			class_id = self.correct_class
			sig = i['sig']
			if i['ClassificationRes'] != class_id:
				print "resolve conflict:", i['sig'], i['returnVar'], class_id
			self.feature_table.update_one({'sig': sig}, {'$set': {'ClassificationRes': class_id}})


	def _handle_fconflict(self, clst):
		print '#########fConflict:', clst

		for func_sig in clst:
			func_pattern = []
			path_lst = []
			for i in self.feature_table.find({'sig': {'$regex': func_sig}}):
				path_lst.append(i)

				tmp = (i['returnType'][0], i['Retrelationtype'])
				if tmp not in func_pattern:
					func_pattern.append(tmp)


			print func_pattern
			func_pattern_match = 0
			func_match = None
			similarity = 0
			for i in self.ret_pattern:
				tmp = []
				for j in i:
					tmp.append(j.keys()[0])

				if all(item in tmp for item in func_pattern):
					tmp_simi = jaccard.similarity(func_pattern, tmp)
					if tmp_simi > similarity:
						similarity = tmp_simi
						func_match = i

					func_pattern_match = 1

			#if func_pattern_match == 0:
			#	for i in path_lst:
			#		print 'use heuristic'
			#		self._heuristic_correct(i)
			#else:
			if func_pattern_match == 1:
				for i in path_lst:
					print 'use cross check:', func_match, i['sig']
					self._cross_check(i, func_match)


	def _handle_mconflict(self, clst):
		print '##########mConflict:', clst

		for func_sig in clst:
			cres = dict()
			func_pattern = []
			conflict_sig = []

			for i in self.feature_table.find({'sig': {'$regex': func_sig}}):
				tmp = (i['returnType'][0], i['Retrelationtype'])
				if tmp not in func_pattern:
					func_pattern.append(tmp)

				if i['mConflict'] == 0:
					continue

				if tmp not in conflict_sig:
					conflict_sig.append(tmp)

				key = i['returnVar'][0]
				if key in cres:
					cres[key].append(i)
				else:
					cres[key] = [i]

			print func_pattern
			func_pattern_match = 0
			func_match = None
			similarity = 0
			for i in self.ret_pattern:
				tmp = []
				for j in i:
					tmp.append(j.keys()[0])

				if all(item in tmp for item in conflict_sig):
					tmp_simi = jaccard.similarity(func_pattern, tmp)
					if tmp_simi > similarity:
						similarity = tmp_simi
						func_match = i

					func_pattern_match = 1

			for k in cres:
				#self._vote_correct(cres[k])

				#class_lst = [i['ClassificationRes'] for i in cres[k]]
				#sig_lst = [i['sig'] for i in cres[k]]
				#print class_lst
				#print sig_lst
				#class_id, freq = Counter(class_lst).most_common(1)[0]

				#if float(freq) / len(class_lst) > 0.6:
				#	print 'use voting'
				#	for idx, sig in enumerate(sig_lst):
				#		if class_lst[idx] != class_id:
				#			print "resolve conflict:", sig, i['returnVar'], class_id
				#		self.feature_table.update_one({'sig': sig}, {'$set': {'ClassificationRes': class_id, 'mConflict': 0, 'fConflict': 0}})

				#else:
				if func_pattern_match == 1:
					for i in cres[k]:
						print 'use cross check:', func_match, i['sig']
						self._cross_check(i, func_match)
				else:
					self._vote_correct(cres[k]) 
					#if self._vote_correct(cres[k]) == False:
					#	for i in cres[k]:
					#		print 'use heuristic'
					#		self._heuristic_correct(i)

					
	def generate_ret_pattern(self):
		ret_pattern = dict()
		res = []
		filter_lst = []

		for i in self.feature_table.find({'$or': [{'mConflict': 1}, {'fConflict': 1}]}):
			if checkSig(self.func_file, i['sig']) == False:
				continue

			m = re.search(r'(.*):', i['sig'])
			filter_lst.append(m.group(1))

		filter_lst = list(set(filter_lst))

		for i in self.feature_table.find({'mConflict':0, 'fConflict':0}):
			sig = i['sig']
			if checkSig(self.func_file, sig) == False:
				continue
			
			m = re.search(r'(.*):', sig)
			fsig = m.group(1)
			
			if fsig in filter_lst:
				continue

			pattern = dict()
			pattern_key = (i['returnType'][0], i['Retrelationtype'])
			pattern_class = i['ClassificationRes']
			pattern[pattern_key] = pattern_class

			if fsig not in ret_pattern:
				ret_pattern[fsig] = [pattern]
			else:
				if pattern not in ret_pattern[fsig]:
					ret_pattern[fsig].append(pattern)


		for func_key in ret_pattern:
			func_pattern = ret_pattern[func_key]
			print func_key, func_pattern

			if not res:
				res.append(func_pattern)
			else:
				flag = 0
				for i in res:
					if len(i) != len(func_pattern):
						continue

					if all(item in i for item in func_pattern):
						flag = 1
						break

				if flag == 0:
					res.append(func_pattern)
		

		#the ret_pattern is a list
		#each item in the list is a list, which represent the function pattern
		#each item in function pattern is a dict, where the key is the (returnType, returnVal), and the value is the corresponding classification result
		#ret_pattern = [ [ {(returnType, returnVal):class}, {...} ], [function pattern], ...]
		self.ret_pattern = res

		print self.ret_pattern


	def final_correct_conflict(self, ctype):
		#IN case we want to run this final correction method along....
		#tc = self._get_label_class('Correct')
		#fc = self._get_label_class('Error')
		#self.correct_class, freq = tc[0]
		#self.error_class, freq = fc[0]

		#self.generate_ret_pattern()

		c_lst = []
		for i in self.feature_table.find({ctype: 1}):
			if checkSig(self.func_file, i['sig']) == False:
				continue

			if i['ClassificationRes'] == self.error_class or i['ClassificationRes'] == self.correct_class:
				m = re.search(r'(.*):(.*)', i['sig'])
				c_lst.append(m.group(1))

		if ctype == 'mConflict':
			print '\n#########mConflict########'
			for mc in list(set(c_lst)):
				print mc

			self._handle_mconflict(list(set(c_lst)))

		if ctype == 'fConflict':
			print '\n#########fConflict##########'
			for fc in list(set(c_lst)):
				print fc

			self._handle_fconflict(list(set(c_lst)))


#correct the conflict in each function based on the returned variable
	def _correct_conflict_wrapper(self, ctype, first=True):
	#NOTE there are two types of conflicts, mconflict and fconflict. 
	
	#For fconflict, which means all paths in a function belong to the same class, we try to:
	#(1) we first look locally to see if the unconflicted paths in the same function have the same returned variables in the conflicted paths. If we have, we resolve the conflicts based the returned variables in the unconflicted paths
	#(2) we vote to determine the error paths. For example, usually there are more error paths in a function than correct paths. If the ratio of those functions with the same returned variable is larger than a certain threshold, we correct the conflict dirrectly
	#(3) we look globally to see if the conflict can be resolved based on the returned variables. For example, if the returned variable 'NULL' appears very often in exising error path class, then we consider the path returns 'NULL' as the error path.
	#(4) if we fail on (1), (2) and (3), we leave them for the next round learning

	#For mconflict, which means the paths with the same returned variable in a function are classified into different classes, we follow the same process above

		c_lst = []
		for i in self.feature_table.find({ctype: 1}):
			if i['ClassificationRes'] == self.error_class or i['ClassificationRes'] == self.correct_class:
				m = re.search(r'(.*):(.*)', i['sig'])
				c_lst.append(m.group(1))

		for func_sig in list(set(c_lst)):
			print func_sig, ctype
			docs = self.feature_table.find({'sig': {'$regex': func_sig}})

			self._retVar_determined(docs)
			docs.rewind()

			self._retVar_voting(docs, ctype, first)
			docs.rewind()

			self._retVar_globalChk(docs, first)


	def correct_conflict(self, first=True):
	#get the returned variable list for each class first
		error_retVar = self._get_class_retVar(self.error_class)
		self.global_errRet = Counter(error_retVar).most_common()
		print 'Error Ret:', self.global_errRet

		correct_retVar = self._get_class_retVar(self.correct_class)
		self.global_correctRet = Counter(correct_retVar).most_common()
		print 'Correct Ret:', self.global_correctRet
		
		self._correct_conflict_wrapper('fConflict', first)

		self._correct_conflict_wrapper('mConflict', first)

	def _get_retVar_2(self, returnType, returnVar):
		retType = returnType[0]
		retVar = returnVar[0].replace(' ', '')

		if retType == FUNC_TYPE:
		#only need function name as the returned variable
			m = re.search(r'\(.*\)', retVar)
			retVar = retVar[:m.start()]

		return retVar


#we only use the function name here, which is different from _get_class_retVar()
	def _get_class_retVar_2(self, class_id):
		res = []
		for i in self.feature_table.find({'ClassificationRes': class_id, 'Determined': 1}):
		        if checkSig(self.func_file, i['sig']) == False:
				continue

			retVar = self._get_retVar_2(i['returnType'], i['returnVar'])

			#if len(i['returnType']) == 1:
			#	retType = i['returnType'][0]
			#	retVar = i['returnVar'][0].replace(' ', '')
			#else:
			#	retType = 3
			#	retVar = ';'.join(i['returnVar']).replace(' ', '')

			#if retType == 1:
			#	m = re.search(r'\(.*\)', retVar)
			#	retVar = retVar[:m.start()]

			res.append(retVar)

		return list(set(res))

	def _get_class_conditions_2(self, class_id):
		res = []
		for i in self.feature_table.find({'ClassificationRes': class_id, 'Determined': 1}):
			conditions = i['conditionals']
			p_conditions = []
			for c in conditions:
				tmp = {c['code']: c['flowAction']}
				p_conditions.append(tmp)

			res.append(p_conditions)

		return res

	def _get_condition_content(self, condition):
		_code = condition['code']
		code = filter_wrapper(_code)
			
		EC = Exp.Compiler()
		try:
			tmp_r = EC.compile(code)
			key = Exp.ifCondition_sig2str(tmp_r)
		except Exception as e:
			#print 'Parsing error:', e
			#print 'Error exp:', _code
			key = code

		return key


	def _get_class_conditions(self, class_id):
		res = {}
		for i in self.feature_table.find({'ClassificationRes': class_id, 'Determined': 1}):
			if checkSig(self.func_file, i['sig']) == False:
				continue

			for c in i['conditionals']:
				#_code = c['code']
				#code = filter_wrapper(_code)
				#EC = Exp.Compiler()
				#try:
				#	tmp_r = EC.compile(code)
				#	key = Exp.ifCondition_sig2str(tmp_r)
				#except Exception as e:
				#	print e
				#	print 'parsing error:', _code
				#	key = code
	
				#key = c['code']
				key = self._get_condition_content(c)
				value = c['flowAction']

				if key in res:
					if res[key] != value:
						res.pop(key, None)
				else:
					res[key] = value

		return res


	def _get_class_funcs(self, class_id):
		res = []
		for i in self.feature_table.find({'ClassificationRes': class_id, 'Determined': 1}):
			if not i['funcLst']:
				continue

			if checkSig(self.func_file, i['sig']) == False:
				continue

			path = i['path']
			for n in reversed(path[:-1]):
				if n['type'] == 'Condition':
				#we only consider the function within the last TAKEN if-condition
					if n['flowAction'] == 'True':
						break
					else:
						continue

				code = n['code']
				fname = get_func_name(code)

				if fname:
					res.append(fname)

		return list(set(res))


	def _newFeature_dbUpdate(self, skip = 0):
		for i in self.feature_table.find():
			if i['SingleRet'] == 1:
				continue

			if skip == 1:
				self.feature_table.update_one({'sig': i['sig']}, {'$set': {'ErrVar': 0, 'CorrectVar': 0, 'ErrCondition': 0, 'CorrectCondition': 0, 'ErrFunc': 0, 'CorrectFunc': 0, 'mConflict': 0, 'fConflict': 0}})
				continue


			#if i['Determined'] == -999:
			if checkSig(self.func_file, i['sig']) == True:
#				class_id = i['ClassificationRes']
#				if class_id == self.error_class:
#					self.feature_table.update_one({'sig': i['sig']}, {'$set': {'ErrVar': 1, 'CorrectVar': 0, 'ErrCondition': 1, 'CorrectCondition': 0, 'ErrFunc': 1, 'CorrectFunc': 0, 'Prelabel': 'Error'}})
#				elif class_id == self.correct_class:
#					self.feature_table.update_one({'sig': i['sig']}, {'$set': {'ErrVar': 0, 'CorrectVar': 1, 'ErrCondition': 0, 'CorrectCondition': 1, 'ErrFunc': 0, 'CorrectFunc': 1, 'Prelabel': 'Correct'}})

#			else:
				#chk the return var first
				errvar = 0
				correctvar = 0
				retVar = self._get_retVar_2(i['returnType'], i['returnVar'])

				if retVar in self.errVar_set and retVar not in self.correctVar_set:
					errvar = 1

				if retVar in self.correctVar_set and retVar not in self.errVar_set:
					correctvar = 1

				#chk the conditions
				errcondition = 0
				correctcondition = 0
				for c in i['conditionals']:
					if errcondition == 1 and correctcondition == 1:
						errcondition = 0
						correctcondition = 0
						break

					key = self._get_condition_content(c)
					value = c['flowAction']

					if key in self.errCondition_set:
						if self.errCondition_set[key] == value:
							errcondition = 1

					if key in self.correctCondition_set:
						if self.correctCondition_set[key] == value:
							correctcondition = 1

				#chk the funcs
				errfunc = 0
				correctfunc = 0

				if i['funcLst']:
					path = i['path']
					for n in reversed(path[:-1]):
						if errfunc == 1 and correctfunc == 1:
							break

						if n['type'] == 'Condition':
							break

						code = n['code']
						fname = get_func_name(code)

						if fname:
							if fname in self.errFunc_set and fname not in self.correctFunc_set:
								errfunc = 1

							if fname in self.correctFunc_set and fname not in self.errFunc_set:
								correctfunc = 1

				#update the db for undetermined record
				#print(errvar, correctvar, errcondition, correctcondition, errfunc, correctfunc)
				self.feature_table.update_one({'sig': i['sig']}, {'$set': {'ErrVar': errvar, 'CorrectVar': correctvar, 'ErrCondition': errcondition, 'CorrectCondition': correctcondition, 'ErrFunc': errfunc, 'CorrectFunc': correctfunc, 'mConflict': 0, 'fConflict': 0}})


        def _save_project_features(self):
            prefix = timeit.default_timer()
            
            #write ret var
            errVarF = 'errVar.' + str(prefix)
            with open(errVarF, 'a') as f:
                for i in list(set(self.errVar_set) - set(self.correctVar_set)):
                    f.write(i + '\n')

            correctVarF = 'correctVar.' + str(prefix)
            with open(correctVarF, 'a') as f:
                for i in list(set(self.correctVar_set) - set(self.errVar_set)):
                    f.write(i + '\n')

            #write last func call
            errFuncF = 'errFunc.' + str(prefix)
            with open(errFuncF, 'a') as f:
                for i in list(set(self.errFunc_set) - set(self.correctFunc_set)):
                    f.write(i + '\n')

            correctFuncF = 'correctFunc.' + str(prefix)
            with open(correctFuncF, 'a') as f:
                for i in list(set(self.correctFunc_set) - set(self.errFunc_set)):
                    f.write(i + '\n')

            #write last if-condition
            errConditionF = 'errCondition.' + str(prefix)
            with open(errConditionF, 'a') as f:
                for k, v in self.errCondition_set.items():
                    f.write(str(k) + ' : ' + str(v) + '\n')

            correctConditionF = 'correctCondition.' + str(prefix)
            with open(correctConditionF, 'a') as f:
                for k, v in self.correctCondition_set.items():
                    f.write(str(k) + ' : ' + str(v) + '\n')
                

	def newFeature_extract(self, skip=0):
		if skip == 1:
			self._newFeature_dbUpdate(skip)
			return

		tc = self._get_label_class('Correct')
		fc = self._get_label_class('Error')
		self.correct_class, freq = tc[0]
		self.error_class, freq = fc[0]

		#(1) two sets to store the returned variables in error path and correct path class
		self.errVar_set = self._get_class_retVar_2(self.error_class)
		self.correctVar_set = self._get_class_retVar_2(self.correct_class)

		#print '########Error variables#######'
		#for vi in self.errVar_set:
		#	print vi
		#print '########Correct variables########'
		#for vi in self.correctVar_set:
		#	print vi

		#(2) two sets to store the if-conditions in error path and correct path class
		_errCondition_set = self._get_class_conditions(self.error_class)
		self.errCondition_set = {}

		_correctCondition_set = self._get_class_conditions(self.correct_class)
		self.correctCondition_set = {}

		#print "#########Error Condition#############"
		for key, value in _errCondition_set.iteritems():
		    if key not in _correctCondition_set:
			self.errCondition_set[key] = value
		    else:
			if _correctCondition_set[key] != value:
			    self.errCondition_set[key] = value
		#for kc, kv in self.errCondition_set.items():
		#	print kc, ':', kv

		#print "########Correct Condition#############"
		for key, value in _correctCondition_set.iteritems():
		    if key not in _errCondition_set:
			self.correctCondition_set[key] = value
		    else:
			if _errCondition_set[key] != value:
			    self.correctCondition_set[key] = value
		#for kc, kv in self.correctCondition_set.items():
		#	print kc, ':', kv

		#(3) two sets to store the called funtions (within the last if-condition) in error path and correct path class
		self.errFunc_set = self._get_class_funcs(self.error_class)
		self.correctFunc_set = self._get_class_funcs(self.correct_class)

		#print '######Error funcs#######'
		#for fi in self.errFunc_set:
		#	print fi
		#print '#########Correct funcs#######'
		#for fi in self.correctFunc_set:
		#	print fi

                self._save_project_features()

		self._newFeature_dbUpdate()

	def final_correct_conflict_1(self):
		#first chk if we have paths that belong to error/correct path class but not determined yet
		for i in self.feature_table.find({'Determined': 0}):
			if i['SingleRet'] == 1:
				continue

			class_id = i['ClassificationRes']
			if class_id == self.error_class or class_id == self.correct_class:
				self.feature_table.update_one({'sig': i['sig']}, {'$set': {'mConflict': 0, 'fConflict': 0, 'Determined': 1}})

		#second chk if we have paths that belong to other classes but their returned variable
		for i in self.feature_table.find({'Determined': 0}):
			if i['SingleRet'] == 1:
				continue

			retVar = i['returnVar']
			refs = self.feature_table.find({'Determined': 1, 'returnVar': retVar})

			errC = 0
			correctC = 0
			for r in refs:
				if r['ClassificationRes'] == self.error_class:
					errC = 1
				elif r['ClassificationRes'] == self.correct_class:
					correctC = 1

			if errC - correctC != 0:
				if errC:
					class_id = self.error_class
				else:
					class_id = self.correct_class

				self.feature_table.update_one({'sig': i['sig']}, {'$set': {'mConflict': 0, 'fConflict': 0, 'Determined': 1, 'ClassificationRes': class_id}})
				





#cp = CorrectProcess()
#cp.store_classification_res()
#cp.init_label_propagate()
#cp.correct_conflict()
#cp.newFeature_extract()


