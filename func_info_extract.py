import sys
import re, subprocess
import ast
from joern.all import JoernSteps
from pymongo import MongoClient
from bson.objectid import ObjectId
import expressions.compiler as Exp
from py2neo.packages.httpstream import http
import copy
import numpy as np
import condition_eval as ife

#set the timeout when querying the neo4j
http.socket_timeout = 60

NEO4J_DB = JoernSteps()
NEO4J_DB.setGraphDbURL('http://localhost:7474/db/data/')
NEO4J_DB.connectToDatabase()

MACRO_TYPE = 0
CONSTANT_TYPE = 1
VARIABLE_TYPE = 2
FUNC_TYPE = 3
OPERATION_TYPE = 4


def if_has_function(code):
	code = code.replace(' ', '')
	return re.search(r'([\w0-9]+)\(.*\)', code)

def get_func_name(code):
	m = if_has_function(code)
	if m:
		return m.group(1)
	else:
		return None

def get_returned_var(code):
	code = code.replace(';', '')
	m = re.search(r'return[\t ]+(.*)', code)
	if not m:
		#something wrong with the return statement
		print 'error ret:', code
		return ['']

	ret_content = m.group(1).replace(' ', '')

	m = re.search(r'^\((.*?)\)$', ret_content)
	if m:
		s = m.start()
		e = m.end() - 1
		in_cnt = 1
		for n, i in enumerate(ret_content[1:], 1):
			if i == '(':
				in_cnt += 1

			if i == ')':
				in_cnt -= 1

			if in_cnt == 0:
				break

		if n == e:
			ret_content = m.group(1)

	m = re.search(r'^([\w0-9]+)\(.*\)', ret_content)
	if m:
		return [ret_content]

	m = re.search(r'\?(.*?):(.*)', ret_content)
	if m:
		return [m.group(1), m.group(2)]
	else:
		#just in case someone writes the return value as "return ret = f(...)"
		m = re.search(r'[\w0-9]+=(?!=)(.*)', ret_content)
		if m:
			return [m.group(1)]
		else:
			return [ret_content]

def if_is_operation(var):
	return re.search(r'[+-/*%!><&|^~]', var)

class FuncInfoExtract():
	def __init__(self, coll, code_base):
		self.code_path = code_base
		self.out_collection = coll
		self.neo4j = NEO4J_DB
		self._mongo_init()

	def _mongo_init(self):
		self.client = MongoClient()
		self.db = self.client.errspec
		self.feature_table = self.db[self.out_collection]
		self.feature_table.create_index('sig')

	def _getCFGEntryNode(self, function_id):
		query = """queryNodeIndex('functionId:%s AND type:CFGEntryNode')""" % (function_id)
		return self.neo4j.runGremlinQuery(query)[0]

    	def _getCFGReturnNodes(self, function_id):
	    	query = """queryNodeIndex('functionId:%s AND type:ReturnStatement')""" % (function_id)
	    	return self.neo4j.runGremlinQuery(query)

	def _getCFGConditionNodes(self, function_id):
		query = """queryNodeIndex('functionId:%s AND type:Condition')""" % (function_id)
		return self.neo4j.runGremlinQuery(query)

	def _getCFGEdges(self, function_id):
		query = """queryNodeIndex('functionId:%s AND isCFGNode:True').outE('FLOWS_TO')""" % (function_id)
		return self.neo4j.runGremlinQuery(query)

	def _getDDGRelation(self, s_id, e_id, type):
		query = """START s=node(%s), d=node(%s) MATCH p=s-[r:%s]->d RETURN EXTRACT(x in RELATIONSHIPS(p) | x);""" % (s_id, e_id, "REACHES")
		try:
			q_res = self.neo4j.graphDb.cypher.execute(query)
		except Exception as e:
			print 'query error:', e
			q_res = None

		if q_res:
			return q_res[0][0][0].get_properties()['var']
		else:
			return None

	def _getPaths_cypher(self, function_id, type):
		#check if it has switch-case in the function or not
		#cfgE = self._getCFGEdges(function_id)
		#for i in cfgE:
		#	edge = i.get_properties()
		#	if "default" in edge['flowLabel'] or 'case' in edge['flowLabel']:
		#		print 'Skip this function because of the unsupported switch-case'
		#		return ([],[])

		#check number of if-conditions we have
		c_cnt = len(self._getCFGConditionNodes(function_id))
		if c_cnt > 20:
		#too many if-conditioins will stuck the query..... sucks. We just skip such function
			print 'Skip this function because of condition number:', str(c_cnt)
			return ([], [])

		#get the entry node first
		s = self._getCFGEntryNode(function_id)
		s_id = s._id

		#get list of return statement
		outs = self._getCFGReturnNodes(function_id)

		paths = []
		ret_path_lst = []
		for d in outs:
			d_id = d._id
			ret_properties = d.get_properties()

			m = re.search(r'(.*):(.*):(.*):(.*):(.*)', node_location(d_id))
			ret_line = int(m.group(2))

			query="""START s=node(%s), d=node(%s) MATCH p=s-[r:%s*..]->d WHERE ALL(n in nodes(p) WHERE 1=length(filter(m in nodes(p) WHERE m=n)))  RETURN EXTRACT(x in NODES(p) | x);""" % (s_id, d_id, type)
			try:
				q_res = self.neo4j.graphDb.cypher.execute(query)
			except Exception as e:
				print 'query error:', e
				q_res = []

			ret_paths = len(q_res)
			ret_path_lst.append(ret_paths)
			#print '# of paths:', ret_paths, ret_properties['code']

			if ret_paths > 128:
			#yeah, too many useless paths, let's sample some
			#in most cases, the returned values of those paths are correct, so it is OK for us to skip part of it.
				sample = ret_paths / 128 + 1
			else:
				sample = 1

			for p in q_res[::sample]:
				paths.append((ret_paths, ret_line, p[0]))

		return (ret_path_lst, paths)

	def _getCFGEdge(self, sid):
		query = """g.v('%s').outE('FLOWS_TO')""" % (sid) 
        	return self.neo4j.runGremlinQuery(query)

    	def _getSymbolNodes(self, function_id):
        	query = """queryNodeIndex('functionId:%s AND type:Symbol')""" % (function_id)
        	return self.neo4j.runGremlinQuery(query)

#Hmmm, this is too slow.... Use cypher to query the data instead...
  	def _getPaths(self, function_id, type):
		query = """queryNodeIndex('functionId:%s AND type:CFGEntryNode').out('%s').loop(1){it.loops<50 && it.object.hasNot('type','InfiniteForNode')}{true}.dedup().has('type','ReturnStatement').path{it}""" % (function_id, type)
		try:
        		res = self.neo4j.runGremlinQuery(query)
		except Exception as e:
			print e
			return []

		print len(res)
		return res

	#the DDG path to a certain node
  	def _getDDG(self, function_id, loc, type):
		query = """queryNodeIndex('functionId:%s AND isCFGNode:True').out('%s').loop(1){it.loops<50}{true}.dedup().has('location','%s').path{it}""" % (function_id, type, loc)
		try:
			res = self.neo4j.runGremlinQuery(query)
		except Exception as e:
			print e
			return []

		return res

	def _merge_DPath(self, p, DP):
		if not DP:
			DP.append(p)
		else:
			#check if p is in DP first and then insert p
			for n, i in enumerate(DP):
				if all(x in i for x in p):
					return
				elif all(x in p for x in i):
					DP[n] = i
				else:
					DP.append(p)

	def _extract_DPath(self, node, function_id):
		DP = []
		
		for p in self._getDDG(function_id, node['location'], 'REACHES'):
			tmp = []
			for n in p:
				tmp.append(n.get_properties())

			self._merge_DPath(tmp, DP)

		return DP

	def _check_data_dependence(self, path, dst):
		DPath = []
		for n in path:
			if n == dst:
				break

			query = """START s=node(%s), d=node(%s) MATCH p=shortestPath(s-[r:%s*..]->d) RETURN EXTRACT(x in NODES(p) | x);""" % (n["nid"], dst["nid"], "REACHES")
			try:
				q_res = self.neo4j.graphDb.cypher.execute(query)
			except Exception as e:
				print 'query error:', e
				q_res = []

			for i in q_res:
				DPath.append(i[0])

		try:
			return min(DPath, key=len)
		except ValueError:
			return []


	def _get_var_type(self, var, path, conditions, ret_range, s=-1):
		ret = []
		rn = path[s]
		#data_path = self._extract_DPath(rn, function_id)
		data_path = self._check_data_dependence(path, rn)

		if not data_path:
			#constant value
			if if_has_function(var):
				ret = [FUNC_TYPE, str(var), ret_range]
			else:
				tmp = str(var)
				m = re.search(r'^(.*),', tmp)
				if m:
					tmp = m.group(1)

				if tmp == 'true' or tmp == 'false':
					v_type = MACRO_TYPE
				else:
					if tmp.isupper():
					#macro type
						m = re.search(r'^([0-9]+)([a-zA-Z]*)$', tmp)
						if not m:
							v_type = MACRO_TYPE
						else:
							v_type = CONSTANT_TYPE

					else:
						try:
							if type(eval(tmp)) != type(len):
								v_type = CONSTANT_TYPE
							else:
								v_type = VARIABLE_TYPE
						except Exception:
							v_type = VARIABLE_TYPE

				ret = [v_type, tmp, ret_range]

		elif if_has_function(var):
			tmp = Exp.exp_signature(var.replace(' ',''))
			if tmp['type'] == 1 or tmp['type'] == -1:
				#function type
				ret = [FUNC_TYPE, str(var), ret_range]
			else:
				ret = [OPERATION_TYPE, str(var), ret_range]

		elif if_is_operation(var):
			#operation type, like (a + b)
			ret = [OPERATION_TYPE, str(var), ret_range]

		else:
			if not if_has_function(var) and not if_is_operation(var):
				ret_range = self._return_range(conditions, var, len(path[:s]))

			#traverse back to see if var is stated along the path
			for idx, val in enumerate(reversed(path[:s]), start=1):
				if val['type'] == "Condition":
					continue

				tmp_regex = str(var) + r'=(?!=)(.*)'
				try:
					match = re.search(tmp_regex, val['code'].replace(' ', ''))
				except Exception as e:
					#print "#####Error:", e, var
					#print val['code']
					return []

				if match:
					#print val['code']
					s = s - idx
					assign_var = match.group(1).replace(';', '')
					ret = self._get_var_type(assign_var, path, conditions, ret_range, s)
					break
	
		#if ret is an empty list, the var is defined somewhere else....
		return ret

	def _get_ConditionAct(self, s, e):
		for outE in self._getCFGEdge(s['nid']):
			if outE.end_node._id == e['nid']:
				return outE.get_properties()['flowLabel']

		#should not be here!!!
		return None

	def _getConditions(self, path_info):
		res = filter(lambda x: x['type'] == 'Condition', path_info)

		for i in res:
			idx = path_info.index(i)
			#next node of the condition in the path
			j = path_info[idx + 1]
			i['flowAction'] = self._get_ConditionAct(i, j)
			i['idx'] = idx

			#check if it is a switch-case, convert it to if condition
			if "default" in i['flowAction']:
				v = i['code']
				i['code'] = str(v) + ' == ELSE'
				i['flowAction'] = "True"
			elif "case" in i['flowAction']:
				case_value = re.search(r'(case) (.*)', i['flowAction']).group(2)
				v = i['code']
				i['code'] = str(v) + ' == ' + str(case_value)
				i['flowAction'] = "True"

		return res

	def _eval_constant(self, var):
		#print 'eval value for:', var

		if var == 'NULL' or var == 'false' or var == 'FALSE':
			return 'negative'

		if var == 'true' or var == 'TRUE':
			return 'positive'

		try:
			value = Exp.EvaluateVar(var, self.code_path)
		except Exception:
			return 'NA'

		if value == None:
		#parsing error.....
			return 'NA'
		else:
			if value == 0:
				return '0'
			elif value >0:
				return 'positive'
			else:
				return 'negative'


	def _chkRetPosType(self, path):
		last_if = 0
		
		goto_label = 0
		loop = 0
		
		for n in reversed(path[:-1]):
			if n['type'] == 'Condition':
				last_if += 1
				
				if last_if == 2:
					query = """g.v('%s').inE('FLOWS_TO')""" % (n['nid'])
					q_res = self.neo4j.runGremlinQuery(query)
					if len(q_res) > 1:
						return 1	#returned in loop
					else:
						return 0	#normal return
					

			elif last_if == 0 and n['type'] == 'GotoStatement':
				return 2	#returned in goto

		return 0

	def _return_range(self, conditions, ret, idx):
		res = ''
		if self._eval_constant(ret) != 'NA':
			return res

		for c in reversed(conditions):
			if c['idx'] > idx:
				continue

			res = ife.eval_condition(ret, c['code'], c['flowAction'])
			if res:
				return res

		return res

	def pathFeature(self, function_id, func_sig, start_line):
		#print function_id

		path_raw_info = {}
		path_id = 0
		max_path_len = max_con_len = max_statement_len = max_func_len = max_ret_line = 0
		ret_relation_type = [] #just contains the return value kind of each path
		fsig = str(func_sig).strip('\n')

		ret_paths, func_paths = self._getPaths_cypher(function_id, 'FLOWS_TO')

    		for ret_path, ret_line, p in func_paths:

			path_info = []
			for node in p:
				properties = node.get_properties()
				properties['nid'] = node._id
				if properties['type'] != 'Parameter': 
					path_info.append(properties)

				if properties['type'] == 'GotoStatement':
					m = re.search(r'(.*):(.*):(.*):(.*):(.*)', node_location(node._id))
					ret_line = int(m.group(2))
					
			path_raw_info['path'] = path_info

			#print path_info
			if len(path_info) - 1 > max_path_len:
				max_path_len = len(path_info) - 1

			path_conditional = self._getConditions(path_info)
			path_raw_info['conditionals'] = path_conditional

			#print path_conditional
			if len(path_conditional) > max_con_len:
				max_con_len = len(path_conditional)

			ret_len = ret_line - start_line
			path_raw_info['returnPos'] = ret_len 
			if ret_len > max_ret_line:
				max_ret_line = ret_len

			#we want to give large value to the case like 9,1,1,1,1, while small value for 2,3,2,3. 
			#DO NOT use the returnPaths. It does not work well with goto...
			path_raw_info['returnPaths'] = (float(ret_path) / max(ret_paths)) * np.std(ret_paths)
			if path_raw_info['returnPaths'] > 1.0:
				path_raw_info['returnPaths'] = 1.0

#			!!!!!! put this into real feature generation code!!!!!!
#			path_condition_lst = [Exp.exp_signature(x['code'], x['flowAction']) for x in path_conditional]
#			path_raw_info['conditionLst'] = path_condition_lst
			
			path_expStatement = filter(lambda x: x['type'] == 'ExpressionStatement', path_info)
			path_raw_info['expStatement'] = path_expStatement

			#print path_expStatement
			if len(path_expStatement) > max_statement_len:
				max_statement_len = len(path_expStatement)
			
			path_func_lst = [get_func_name(x['code']) for x in path_info if x['type'] != 'Condition' and if_has_function(x['code'])]
			path_raw_info['funcLst'] = path_func_lst

			path_raw_info['retPosType'] = self._chkRetPosType(path_info)

			#print path_func_lst
			if len(path_func_lst) > max_func_len:
				max_func_len = len(path_func_lst)
			
#			path_symbols = [x.get_properties()['code'] for x in self._getSymbolNodes(function_id)]
			
			path_return_var = get_returned_var(path_info[-1]['code'])
			if path_return_var == ['']:
			#The function is void type, but it has return. Skip such case
				continue

			path_return_type = []
			path_return_range = [''] * len(path_return_var)
			#there may have two return values
			for idx, ret in enumerate(path_return_var):
#				print '#########'
#				print 'ret: ' + ret
				#check the type of ret
				#print '----ret var:'
				ret_type = self._get_var_type(ret, path_info, path_conditional, path_return_range[idx])
				#print 'end-----'

				if not ret_type:
					if ret == 'true' or ret == 'false':
						infer_type = MACRO_TYPE
						ret_relation_type.append(self._eval_constant(ret))
					#returned type is defined somewhere else or an error happened when parsing the expression....
					else:
						if ret.isupper():
						#macro....
							m = re.search(r'^([0-9]+)([a-zA-Z]*)$', ret)
							if not m:
								infer_type = MACRO_TYPE
							else:
								infer_type = CONSTANT_TYPE
						
							ret_relation_type.append(self._eval_constant(ret))
						#print self._eval_constant(ret)
						else:
							try:
								if type(eval(ret)) != type(len):
									infer_type = CONSTANT_TYPE
								else:
									infer_type = VARIABLE_TYPE
							except Exception:
								infer_type = VARIABLE_TYPE

							if infer_type == CONSTANT_TYPE:
								ret_relation_type.append(self._eval_constant(ret))
							else:
								ret_relation_type.append('NA')

					path_return_type.append(infer_type)

#					print 'ret range fixed'
#					print 'ret_var: ' + path_return_var[idx]
#					print 'ret_val: ' + ret_relation_type[-1]
				else:
					path_return_type.append(ret_type[0])
					path_return_var[idx] = ret_type[1].replace(' ', '')
					path_return_range[idx]=ret_type[2]

					if ret_type[0] == CONSTANT_TYPE or ret_type[0] == MACRO_TYPE:
						#print self._eval_constant(path_return_var[idx])
						ret_relation_type.append(self._eval_constant(path_return_var[idx]))
					else:
						ret_relation_type.append('NA')

#					print 'ret range ' + ret_type[2]
#					print 'ret_var: ' + path_return_var[idx]
#					print 'ret_val: ' + ret_relation_type[-1]

			if len(path_return_var) == 1:
				#path signature format: "file_path : line_number : function_name : path_id"
				path_sig = fsig + ':' + str(path_id)
				path_raw_info['sig'] = path_sig

				path_raw_info['returnVar'] = path_return_var
				path_raw_info['returnType'] = path_return_type
				path_raw_info['returnRange'] = path_return_range
			
			#the feature raw information of each path is stored in DB
			#OK, we are in a loop, so the _id generated by pymongo will be the same by default...
				path_raw_info['_id'] = ObjectId()	
				self.feature_table.insert_one(path_raw_info)

				path_id = path_id + 1
			
			else:
				extend_condition = dict()
				extend_condition['code'] = 'RETURN_TEST'
				extend_condition['type'] = 'Condition'
				path_raw_info['conditionals'].append(extend_condition)

				for n, v in enumerate(path_return_var):
					if n == 0:
						extend_condition['flowAction'] = 'True'
					else:
						extend_condition['flowAction'] = 'False'

					path_sig = fsig + ':' + str(path_id)
					path_raw_info['sig'] = path_sig

					path_raw_info['returnVar'] = [path_return_var[n]]
					path_raw_info['returnType'] = [path_return_type[n]]
					path_raw_info['returnRange'] = [path_return_range[n]]

					path_raw_info['_id'] = ObjectId()	
					self.feature_table.insert_one(path_raw_info)
				
					path_id = path_id + 1
					

		#check if the returnVar is the same for all path. 
		varLst = []
		for i in range(path_id):
			index = fsig + ':' + str(i)
			doc = self.feature_table.find_one({'sig': index})
			for v in doc['returnVar']:
				varLst.append(str(v))

		if len(list(set(varLst))) == 1 or len(func_paths) == 1:
		#if len(func_paths) == 1:
			singleRet = 1
#			self.feature_table.delete_many({'sig': {'$regex': fsig}})
		else:
			singleRet = 0

		#check the returned varaible relation type
		reltype_set = set(ret_relation_type)

		for n, i in enumerate(ret_relation_type):
			if i == '0':
				ret_relation_type[n] = 0
			elif i == 'positive':
				ret_relation_type[n] = 1
			elif i == 'negative':
				ret_relation_type[n] = 2
			else:
				ret_relation_type[n] = 3

		#update the feature_table for value normalization
		#we still store those functions only have one returned value in DB for error specification collection
		#field 'Determined' is used to indicate if this record has been determined its class
		#field 'Prelabel' is 'NaN' if we do not label the path. Otherwise, it is 'Error' or 'Correct' for error or nonerror path.
		for i in range(path_id):
			index = fsig + ':' + str(i)
			self.feature_table.update_one({'sig': index}, {'$set': {'SingleRet': singleRet, 'Plen': max_path_len, 'Conlen': max_con_len, 'Statelen': max_statement_len, 'Funclen': max_func_len, 'Retpos': max_ret_line, 'Determined': 0, 'Prelabel': 'NaN', 'Retrelationtype': ret_relation_type[i]}})


def node_location(nid):
	nodeId = nid

        query = """idListToNodes(%s)
        .ifThenElse{it.type == 'Function'}{
         it.sideEffect{loc = it.location; }.functionToFile()
         .sideEffect{filename = it.filepath; }
         }{
           it.ifThenElse{it.type == 'Symbol'}
           {
             it.transform{ g.v(it.functionId) }.sideEffect{loc = it.location; }
             .functionToFile()
             .sideEffect{filename = it.filepath; }
            }{
             it.ifThenElse{it.isCFGNode == 'True'}{
               it.sideEffect{loc = it.location}
               .functions().functionToFile()
               .sideEffect{filename = it.filepath; }
             }{
              // AST node
              it.statements().sideEffect{loc = it.location; }
              .functions()
              .functionToFile().sideEffect{filename = it.filepath; }
              }
           }
        }.transform{ filename + ':' + loc }
        
        """ % (nodeId)

	return NEO4J_DB.runGremlinQuery(query)[0]

def get_code_info(funclist_file, db_collection, code_base):
	extractor = FuncInfoExtract(db_collection, code_base)

	with open(funclist_file) as fp:
		for line in fp:
			m = re.search(r'(.*)@(.*):(.*)', line)
			func_name = m.group(1)
			func_path = m.group(2)
			func_line = m.group(3)

			query = """queryNodeIndex('type:Function AND name:%s')""" % (func_name)
			for i in NEO4J_DB.runGremlinQuery(query):
#				print i.get_properties()
				func_id = i._id

				loc_path = re.search(r'(.*):(.*):(.*):(.*):(.*)', node_location(func_id))
				if loc_path.group(1) == func_path:
					print func_name, func_path, func_line
					extractor.pathFeature(func_id, line, int(loc_path.group(2)))
					#print(loc_path.group() + " :OK")
				#	print "===============>END\n"
					
					break



