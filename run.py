# -*- encoding: utf-8 -*-

from func_info_extract import get_code_info
from feature_generation import FeatureGeneration
from ml import run_unsupervised_ml, run_supervised_ml, run_selflearning
from set_label import set_prelabel
from correct_process import CorrectProcess
import sys
from subprocess import call
from db_res_chk import show_path_classification, detect_bugs
import timeit

skip_profea = 0
use_propagation = 1
selflearn = 1

collection_name = sys.argv[1]
project_name = sys.argv[2]
code_base = sys.argv[3]

func_lst = project_name + '.lst'
func_idx = project_name + '.idx'
func_basic_csv = project_name + '.csv'
func_extend_csv = project_name + '_ext.csv'
func_label = project_name + '.label'
func_res = project_name + '.res'

start_time = timeit.default_timer()

fg = FeatureGeneration(collection_name, project_name)
#cp = CorrectProcess('log_ML.res', func_idx, func_lst, collection_name)
cp = CorrectProcess(func_res, func_lst, collection_name)

'''
#extract function information into DB
get_code_info(func_lst, collection_name, code_base)
code_parse_time = timeit.default_timer() - start_time
print 'parse code time:', code_parse_time
start_time = timeit.default_timer()
print '#####################################################'
'''

#generate basic feature
fg.feature_gen_init()
general_feature_gen_time = timeit.default_timer() - start_time
print 'general feature generation time:', general_feature_gen_time
start_time = timeit.default_timer()
print '#####################################################'

#run ML for the 1st round learning
run_unsupervised_ml(func_basic_csv, 1)
clustering_time = timeit.default_timer() - start_time
print 'DBSCAN time:', clustering_time
start_time = timeit.default_timer()
print '#####################################################'

call('cp log_ML.res log_ML.res.1', shell=True)
cmd = """paste -d '~' %s log_ML.res > %s""" % (func_idx, project_name + '.res')
call(cmd, shell=True)

#set initial labels
set_prelabel(func_label, collection_name)

#based on the 1st round learning, correct the conflict and extract new features
cp.store_classification_res(1)
cp.label_propagate(use_propagation)
label_propagate_time = timeit.default_timer() - start_time
print 'propagate label time:', label_propagate_time
start_time = timeit.default_timer()
print '#####################################################'

cp.set_pathLabel()

show_path_classification(collection_name, project_name, 1)

############self learning###############
n_iter = 0
all_labeled = 0

while n_iter < 50:
#    cp.newFeature_extract(skip_profea)

    res = fg.supervised_feature_gen(True, 2)
    res = fg.supervised_feature_gen(False, 2)
    if res == 0:
    #we have labeled all paths
        print 'label all paths within iterations'
        all_labeled = 1
        break

    func_train_csv = project_name + '_train.csv'
    func_test_csv = project_name + '_test.csv'

    res = run_selflearning(func_train_csv, func_test_csv)
    if res == 0:
        print 'no more paths can be learned'
        break
    
    cmd = """sh supervised_res_gen.sh %s 1""" % (project_name)
    call(cmd, shell=True)

    cp.store_classification_res(3)
    cp.set_pathLabel()

    n_iter += 1

if all_labeled == 0:
    print 'unlabel paths left after iterations'

    cp.newFeature_extract(skip_profea)

    fg.supervised_feature_gen(train=True)
    fg.supervised_feature_gen(train=False)

    func_train_csv = project_name + '_train.csv'
    func_test_csv = project_name + '_test.csv'

    run_supervised_ml(func_train_csv, func_test_csv, skip_profea)
    
    cmd = """sh supervised_res_gen.sh %s 0""" % (project_name)
    call(cmd, shell=True)
    
    cp.store_classification_res(2)

#cp.generate_ret_pattern()
#cp.final_correct_conflict('mConflict')
#cp.final_correct_conflict('fConflict')
#resolve_conflict_time = timeit.default_timer() - start_time
#print 'resolve conflict time:', resolve_conflict_time

show_path_classification(collection_name, project_name, 2)

'''
cp.newFeature_extract(skip_profea)

#2nd round learning, use supervised learning method
#####################################
#SUPERVISED
#
fg.supervised_feature_gen(train=True)
fg.supervised_feature_gen(train=False)
project_feature_gen_time = timeit.default_timer() - start_time
print 'project feature generation time:', project_feature_gen_time
start_time = timeit.default_timer()
print '#####################################################'

func_train_csv = project_name + '_train.csv'
func_test_csv = project_name + '_test.csv'

run_supervised_ml(func_train_csv, func_test_csv, skip_profea)
classification_time = timeit.default_timer() - start_time
print 'supervised learning time:', classification_time
start_time = timeit.default_timer()
print '#####################################################'

cmd = """sh supervised_res_gen.sh %s""" % (project_name)
call(cmd, shell=True)

cp.store_classification_res(2)
#show_path_classification(collection_name, project_name, 3)

cp.generate_ret_pattern()
cp.final_correct_conflict('mConflict')
cp.final_correct_conflict('fConflict')
#resolve_conflict_time = timeit.default_timer() - start_time
#print 'resolve conflict time:', resolve_conflict_time

show_path_classification(collection_name, project_name, 2)

detect_bugs(collection_name, project_name, code_base)
'''

print 'Done!'

'''
#
####################################

#generate extended features
#fg.extend_feature_gen()

#run ML for the 2nd round learning
#run_unsupervised_ml(func_extend_csv, 2)
call('cp log_kmeans.txt log_kmeans.txt.1', shell=True)

#for the 2nd round learning, correct the conflict
cp.store_classification_res()
cp.init_label_propagate()
cp.correct_conflict(first=False)
cp.final_correct_conflict()

call('cp log_ML.res log_ML.res.1', shell=True)

show_path_classification(collection_name, 2)
'''

