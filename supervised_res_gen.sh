#!/bin/bash

rm -f ml.res

#tail -n +2 func_train.csv | while read line
tail -n +2 $1_train.csv | while read line
do
	echo $line | cut -d ',' -f 1 >> ml.res
done

#paste -d '~' func_train.idx ml.res > ml.tmp
paste -d '@' $1_train.idx ml.res > ml.tmp

if [ $2 = '0' ]; then
#paste -d '~' func_test.idx log_supervised.res >> ml.tmp
	paste -d '@' $1_test.idx log_supervised.txt >> ml.tmp
else
	paste -d '@' $1_test.idx log_supervised.txt > ml_test.tmp
	grep ';true;[0-9]\.[0-9]*$' ml_test.tmp >> ml.tmp
	sed -i 's/;true;[0-9]\.[0-9]*$//' ml.tmp

	#store the newly learned path results
	suffix=`date +'%s'`
	grep ';true;' ml_test.tmp > new_learned_paths.$suffix
	#sed -i 's/;true;[0-9]\.[0-9]*$//' new_learned_paths.$suffix
fi

sort ml.tmp > ml.merge

cat ml.merge | cut -d '@' -f 1,2 > $1.idx
cat ml.merge | cut -d '@' -f 3 > log_ML.res

cp log_ML.res log_ML.res.2
paste -d '~' $1.idx log_ML.res > $1.res
