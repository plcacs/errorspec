# -*- coding: UTF-8 -*-

import sys, os
import re
import expressions.compiler as Exp
from pymongo import MongoClient
from subprocess import call

reload(sys)
sys.setdefaultencoding('utf-8')

def generate_path(path_info, of):
	path = ''
	for n in path_info:
		code = n['code'].replace(';','')
		type = n['type']
		if type == 'Condition':
			tmp = code + ' (' + n['flowAction'] + ')' + ' --> '
		else:
			tmp = code + ' --> '

		path = path + tmp

	path = path + '$END\n'

	with open(of, 'a') as outfile:
		outfile.write(path)


class FeatureGeneration():
	def __init__(self, coll_n, proj_n):
		self._client = MongoClient()
		self._db = self._client.errspec
		self.feature_table = self._db[coll_n]
		self.proj = proj_n

	def _outfile_setup(self, suffix='', head=0):
		of_name = self.proj + suffix + '.csv'
		try:
			os.remove(of_name)
		except Exception:
			pass

		of_index = self.proj + suffix + '.idx'
		try:
			os.remove(of_index)
		except Exception:
			pass

		of_path = self.proj + suffix + '.path'
		try:
			os.remove(of_path)
		except Exception:
			pass

		with open(of_name, 'a') as out_file:
#			if universal features:
			if head == 0:
				out_file.write('f1,f2,f3,f4,f5,f6,f7\n')
                        elif head == 1:
				out_file.write('target,f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,f11\n')
                        else:
				out_file.write('target,f1,f2,f3,f4,f5,f6,f7\n')
#				if suffix == '_ext':
				#if head == 2:
				#	out_file.write('f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,f11\n')
				#else:
				#	out_file.write('target,f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,f11\n')

		return (of_name, of_index, of_path)

	def _get_basic_feature(self, path):
		#plen = len(path['path']) - 1 
		#mplen = path['Plen']
		
		#clen = len(path['conditionals'])
		#mclen = path['Conlen']

		#slen = len(path['expStatement'])
		#mslen = path['Statelen']

		#flen = len(path['funcLst'])
		#mflen = path['Funclen']

		retType = path['returnType'][0]
		retRelation = path['Retrelationtype']
			
		plen = (float)(len(path['path']) - 1) / path['Plen']

		if path['Conlen'] != 0:
			clen = (float)(len(path['conditionals'])) / path['Conlen']
			#it is larger than 1 because we have the (a?b:c) at the return statement. Ignore it....
			if clen > 1:
				clen = 1.0
		else:
			clen = 0

#		if path['Conlen'] != 0:
#			true_con = [i for i in path['conditionals'] if i['flowAction'] == 'True']
#			false_con = [i for i in path['conditionals'] if i['flowAction'] == 'False']

#			tclen = (float)(len(true_con))  / path['Conlen']
#			if tclen > 1:
#			#it is larger than 1 because we have the (a?b:c) at the return statement. Ignore it....
#				tclen = 1.0
#
#			fclen = (float)(len(false_con))  / path['Conlen']
#			if fclen > 1:
#				fclen = 1.0
#		else:
#			tclen = fclen = 0
			
		if path['Statelen'] != 0:
			slen = (float)(len(path['expStatement'])) / path['Statelen']
		else:
			slen = 0

		if path['Funclen'] != 0:
			flen = (float)(len(path['funcLst'])) / path['Funclen']
		else:
			flen = 0

		retPos = (float)(path['returnPos']) / path['Retpos']

#		return (plen, tclen, fclen, slen, flen, retPos, retType, retRelation)
		return (plen, clen, slen, flen, retPos, retType, retRelation)


	def _get_extend_feature(self, path):
		errVar = path['ErrVar']
		correctVar = path['CorrectVar']
		errCondition = path['ErrCondition']
		correctCondition = path['CorrectCondition']
		errFunc = path['ErrFunc']
		correctFunc = path['CorrectFunc']
		#retPostype = path['retPosType']

		#return str(errVar) + ',' + str(correctVar) + ',' + str(errCondition) + ',' + str(correctCondition) + ',' + str(errFunc) + ',' + str(correctFunc)
		return str(errCondition) + ',' + str(correctCondition) + ',' + str(errFunc) + ',' + str(correctFunc) + ',' + str(errVar) + ',' + str(correctVar)


	def _checkSig(self, sig):
		idx_file = self.proj + '.lst'
		
		m = re.search(r'(.*):', sig)
		fsig = m.group(1)

		with open(idx_file) as f:
			if fsig in f.read():
				return True

		return False

	def supervised_feature_gen(self, train=True, head=1):
                res = 0

		if train:
			of_name, of_index, of_path = self._outfile_setup('_train', head)
		else:
			of_name, of_index, of_path = self._outfile_setup('_test', head)


		for path in self.feature_table.find():
			if path['SingleRet'] == 1:
				continue

			#if path['Determined'] == 'NaN' and self._checkSig(path['sig']) == False:
                        #if self._checkSig(path['sig']) == False:
                        if (path['Prelabel'] != 'Correct' or path['Prelabel'] != 'Error') and self._checkSig(path['sig']) == False:
                                continue

			if train == True:
				if path['Determined'] != 1:
					continue
			else:
				if path['Determined'] == 1:
					continue

#			plen, tclen, fclen, slen, flen, retPos, retType, retRelation = self._get_basic_feature(path)
#			basic_feature = str(plen) + ',' + str(tclen) + ',' + str(fclen) + ',' + str(slen) + ',' + str(flen) + ',' + str(retPos) + ',' + str(retType) + ',' + str(retRelation)
			plen, clen, slen, flen, retPos, retType, retRelation = self._get_basic_feature(path)
                        if head == 1:
			    basic_feature = str(plen) + ',' + str(clen) + ',' + str(slen) + ',' + str(flen) + ',' + str(retPos) 
                        else:
			    basic_feature = str(plen) + ',' + str(clen) + ',' + str(slen) + ',' + str(flen) + ',' + str(retPos) + ',' + str(retType) + ',' + str(retRelation)

			class_id = str(path['ClassificationRes'])
                        res = 1

			with open(of_name, 'a') as out_file:
                            if head == 1:
                                out_file.write(class_id + ',' + basic_feature + ',' + self._get_extend_feature(path) + '\n')
                            else:
                                out_file.write(class_id + ',' + basic_feature + '\n')

			with open(of_index, 'a') as outfile:
				outfile.write(str(path['sig']) + ';' + str(path['returnVar']) + ';' + str(path['returnType']) + ';' + str(path['Retrelationtype']) + '\n')

			generate_path(path['path'], of_path)

                return res

	
	def feature_gen_init(self):
		of_name, of_index, of_path = self._outfile_setup()

		for path in self.feature_table.find():
			if path['SingleRet'] == 1:
				continue

#			plen, tclen, fclen, slen, flen, retPos, retType, retRelation = self._get_basic_feature(path)
#			basic_feature = str(plen) + ',' + str(tclen) + ',' + str(fclen) + ',' + str(slen) + ',' + str(flen) + ',' + str(retPos) + ',' + str(retType) + ',' + str(retRelation)
			plen, clen, slen, flen, retPos, retType, retRelation = self._get_basic_feature(path)
			basic_feature = str(plen) + ',' + str(clen) + ',' + str(slen) + ',' + str(flen) + ',' + str(retPos) + ',' + str(retType) + ',' + str(retRelation)

			#print path['sig'], plen, clen, slen, flen, retType

			with open(of_name, 'a') as out_file:
			    out_file.write(basic_feature + '\n')

			with open(of_index, 'a') as outfile:
			    outfile.write(str(path['sig']) + ';' + str(path['returnVar']) + ';' + str(path['returnType']) + ';' + str(path['Retrelationtype']) + '\n')

			generate_path(path['path'], of_path)


#fg = FeatureGeneration()
#fg.feature_gen_init()
#fg.extend_feature_gen()
