import re

# !var
def if1(var, code, action):
	res = ''

	regex = r'\![^\S]*' + var
	m = re.search(regex, code)
	if m:
		if action == 'True':
			#print '0'
			res = '0'
		else:
			#print '!= 0'
			res = '! 0'

		return [1, res]
	else:
		return [0, res]

# var == res
def if2(var, code, action):
	res = ''

	regex = var + r'[^\S]*==[^\S]*(.*?) (.*)'
	m = re.search(regex, code)
	if m:
		if action == 'True':
			#print m.group(1)
			res = str(m.group(1))
		else:
			#print 'not ' + m.group(1)
			res = '! ' + str(m.group(1))

		return [1, res]
	else:
		return [0, res]

# var != res
def if3(var, code, action):
	res = ''

	regex = var + r'[^\S]*!=[^\S]*(.*?) (.*)'
	m = re.search(regex, code)
	if m:
		if action == 'True':
			#print 'not ' + m.group(1)
			res = '! ' + str(m.group(1))
		else:
			#print m.group(1)
			res = str(m.group(1))

		return [1, res]
	else:
		return [0, res]

# var > res
def if4(var, code, action):
	res = ''

	regex = var + r'[^\S]*>[^\S]*(.*?) (.*)'
	m = re.search(regex, code)
	if m:
		if action == 'True':
			#print '> ' + m.group(1)
			res = '> ' + str(m.group(1))
		else:
			#print '<=' + m.group(1)
			res = '<= ' + str(m.group(1))

		return [1, res]
	else:
		return [0, res]

# var >= res
def if5(var, code, action):
	res = ''

	regex = var + r'[^\S]*>=[^\S]*(.*?) (.*)'
	m = re.search(regex, code)
	if m:
		if action == 'True':
			#print '>= ' + m.group(1)
			res = '>= ' + str(m.group(1))
		else:
			#print '<' + m.group(1)
			res = '< ' + str(m.group(1))

		return [1, res]
	else:
		return [0, res]

# var < res
def if6(var, code, action):
	res = ''

	regex = var + r'[^\S]*<[^\S]*(.*?) (.*)'
	m = re.search(regex, code)
	if m:
		if action == 'True':
			#print '< ' + m.group(1)
			res = '< ' + str(m.group(1))
		else:
			#print '>=' + m.group(1)
			res = '>= ' + str(m.group(1))

		return [1, res]
	else:
		return [0, res]

# var <= res
def if7(var, code, action):
	res = ''

	regex = var + r'[^\S]*<=[^\S]*(.*?) (.*)'
	m = re.search(regex, code)
	if m:
		if action == 'True':
			#print '<= ' + m.group(1)
			res = '<= ' + str(m.group(1))
		else:
			#print '>' + m.group(1)
			res = '> ' + str(m.group(1))

		return [1, res]
	else:
		return [0, res]

def if8(var, code, action):
	if action == 'True':
		#print '!= 0'
		res = '! 0'
	else:
		#print '0'
		res = '0'

	return [1, res]

def check_condition_ret(ret):
	if ret[-1] == ')' and re.search(r'\((.*?\))', ret) == None:
		return ret[:-1]
	else:
		return ret


def eval_condition(var_orig, code, action):
	code += ' '
	var = re.escape(var_orig)

	regex = r'\b' + var + r'\b'
	try:
		m = re.search(regex, code)
	except Exception:
		return ''

	if not m:
		#print 'not found\n'
		return ''
	else:
	# KEEP THE ORDER!!!!
		#print var, code, action

		tmp = if1(var, code, action)
		if tmp[0]:
			return check_condition_ret(tmp[1])
	
		tmp = if2(var, code, action)
		if tmp[0]:
			return check_condition_ret(tmp[1])

		tmp = if3(var, code, action)
		if tmp[0]:
			return check_condition_ret(tmp[1])

		tmp = if5(var, code, action)
		if tmp[0]:
			return check_condition_ret(tmp[1])
		
		tmp = if4(var, code, action)
		if tmp[0]:
			return check_condition_ret(tmp[1])
		
		tmp = if7(var, code, action)
		if tmp[0]:
			return check_condition_ret(tmp[1])

		tmp = if6(var, code, action)
		if tmp[0]:
			return check_condition_ret(tmp[1])
		
		tmp = if8(var, code, action)
		return check_condition_ret(tmp[1])

