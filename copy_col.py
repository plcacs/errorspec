from pymongo import MongoClient
import sys

proj = sys.argv[1]

client = MongoClient()
db = client.errspec
src = db['zlib']
dst = db[proj]

for i in src.find():
	dst.insert(i)
