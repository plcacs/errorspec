#!/bin/bash

#$1 - sw project dir; $2 - function list output file 
if [ ! $# -eq 2 ]; then
	echo "need the paring dir name and the output file name"
	exit 1
fi

>$2

for f in `find $1 -type f -name "*.c"`
do
	ctags -x --c-kinds=f $f | while read line
	do
#We have code in func_info_extract.py to handle the void function, which is more accurate
#		flag=0
#		tmp=`cut -d '(' -f 1 <<< $line`
#		for i in $tmp
#		do
#			if [ "$i" = "void" ]; then
#				flag=1
#			fi
#		done

#		if [ $flag -eq 1 ]; then
#			echo $line
#			echo "the return type is void, skip it..."
#			continue
#		fi

		func_name=`awk '{print $1}' <<< $line`
		line_no=`awk '{print $3}' <<< $line`
		file_name=`awk '{print $4}' <<< $line`
		func_sig=$func_name@$file_name:$line_no
		echo $func_sig

#		if grep -Fxq $func_sig finish.list
#		then
#			echo "already parsed..."
#			continue
#		fi

		echo $func_sig >> $2

#Do not use the Joern shelltool to query the information, it sucks!
#		echo 'type:Function AND name:'$func_name | joern-lookup | while read line
#		do
#			tmp=`echo $line | joern-location | cut -d ':' -f 1`
#			if [ "$tmp" = "$PWD/$file_name" ]; then
#				echo "Got it: "$line
				#extract features
#				func_id=$line
#				echo $func_id $2 $func_sig:$func_name | python feature_extract.py

#				echo "OK" 
#				break
#			fi
#		done

		echo $func_sig >> finish.list
	done
done

