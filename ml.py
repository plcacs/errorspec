from patsy import dmatrix, dmatrices
import numpy as np
import pandas as pd
import os
import random
from scipy.stats import hmean

from sklearn.cluster import DBSCAN
from sklearn.cluster import MeanShift
from sklearn import metrics
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MaxAbsScaler
from sklearn.cluster import estimate_bandwidth
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC

from sklearn.cluster import KMeans

from sklearn.ensemble import RandomForestClassifier

from dbscan import MyDBSCAN
from sklearn.preprocessing import OneHotEncoder

#############################################################################
def feature_sample(X1, X2, seed):
    #c = [0,1,2,3,4,5,9,13]
    np.random.seed(seed)
    random.seed(seed)
    uf = [0,1,2,3,4]
    uf_n = np.random.randint(3, 5)
    index_uf = random.sample(uf, uf_n)

    np.random.seed(seed)
    random.seed(seed)
    pf = [5,9,13]
    pf_n = np.random.randint(1, 3)
    index_pf = random.sample(pf, pf_n)

    index = index_uf
    for i in index_pf:
        index += [i, i+1, i+2, i+3]

    index.sort()
    print index

    return X1[:,index], X2[:,index]


def data_preprocess(X, n_values, scale = 1.0, skip = 0):
	split_pos = 5
	X = X.values
	X_r = MaxAbsScaler().fit_transform(X[:, 0:split_pos])

#	n_values = [5,4,2,2,2,2,2,2]
#	n_values = [5,2,2,2,2,2,2]
#	enc = OneHotEncoder(n_values = n_values[:X.shape[1]-split_pos])
	enc = OneHotEncoder(n_values)
	X_n = enc.fit_transform(X[:, split_pos:X.shape[1]]).toarray() * scale

	X_m = np.concatenate((X_r, X_n), axis=1)

	if skip:
		return X_r
	else:
		return X_m

def run_unsupervised_ml(feature_file, itern):
	data = pd.read_csv(feature_file)
	if itern == 1:
		X = dmatrix('f1+f2+f3+f4+f5+f6+f7 - 1', data, return_type='dataframe')
	else:
		X = dmatrix('f1+f2+f3+f4+f5+f6+f7+f8+f9+f10+f11+f12 - 1', data, return_type='dataframe')

	X = data_preprocess(X, [5,4])
	#X = StandardScaler().fit_transform(X)
	#for i in X:
	#	print i

	#bandwidth = estimate_bandwidth(X)
	#ml = MeanShift(bandwidth=bandwidth, bin_seeding=True).fit(X)
	#labels = ml.labels_
	#n_clusters_ = len(np.unique(labels))

	#ml = DBSCAN(eps=0.3).fit(X)
	#labels = ml.labels_
	#n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
	#print 'ML parameters:', ml.get_params()

	labels = MyDBSCAN(X, eps=0.3, MinPts=15)
	n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

	try:
		os.remove('log_ML.res')
	except Exception:
		pass

	with open('log_ML.res', 'a') as outfile:
		print('########ML Result#######\n')
		print('Estimated number of clusters: %d\n' % n_clusters_)
		#print("Silhouette Coefficient: %0.3f\n" % metrics.silhouette_score(X, labels))

		for i in labels:
			outfile.write(str(i) + '\n')

	#we can use kmeans to test against dbscan with better fine-grain
	#kmeans = KMeans(n_clusters=n_clusters_, random_state=0).fit(X)
	#try:
	#	os.remove('log_kmeans.txt')
	#except Exception:
	#	pass

	#with open('log_kmeans.txt', 'a') as outfile:
	#	for i in kmeans.labels_:
	#		outfile.write(str(i) + '\n')


def run_supervised_ml(train_file, test_file, skip=0):
	tr_data = pd.read_csv(train_file)
	test_data = pd.read_csv(test_file)

	tr_y, tr_X = dmatrices('target~f1+f2+f3+f4+f5+f6+f7+f8+f9+f10+f11 - 1', tr_data, return_type='dataframe')
	test_y, test_X = dmatrices('target~f1+f2+f3+f4+f5+f6+f7+f8+f9+f10+f11 - 1', test_data, return_type='dataframe')

	n_values = [2,2,2,2,2,2]
	tr_y = np.ravel(tr_y)
	tr_X = data_preprocess(tr_X, n_values, 1.0, skip)

	test_y = np.ravel(test_y)
	test_X = data_preprocess(test_X, n_values, 1.0, skip)

	model_lr = LogisticRegression(solver='sag', max_iter=5000)
	model_svc = SVC(kernel="poly", degree=2)
	model_dt = DecisionTreeClassifier(criterion='entropy')
	model_rf = RandomForestClassifier(n_estimators=500, random_state=1)

	model = model_rf
	clf = model.fit(tr_X, tr_y)

	predict_res = clf.predict(test_X)
        print len(predict_res)

        predict_prob = clf.predict_proba(test_X)

        print(clf.feature_importances_)

	try:
		os.remove('log_supervised.txt')
	except Exception:
		pass

	with open('log_supervised.txt', 'a') as outfile:
	    for n, i in enumerate(predict_res):
                prob = max(predict_prob[n])
		outfile.write(str(int(i)) + '\n')


def run_selflearning(train_file, test_file, skip=0):
	tr_data = pd.read_csv(train_file)
	test_data = pd.read_csv(test_file)

	tr_y, tr_X = dmatrices('target~f1+f2+f3+f4+f5+f6+f7 - 1', tr_data, return_type='dataframe')
	test_y, test_X = dmatrices('target~f1+f2+f3+f4+f5+f6+f7 - 1', test_data, return_type='dataframe')

#	n_values = [2,2,2,2,2,2]
	tr_y = np.ravel(tr_y)
#	tr_X = data_preprocess(tr_X, n_values, 1.0, skip)
	tr_X = data_preprocess(tr_X, [5,4])

	test_y = np.ravel(test_y)
#	test_X = data_preprocess(test_X, n_values, 1.0, skip)
	test_X = data_preprocess(test_X, [5,4])

#        tr_X_1, test_X_1 = feature_sample(tr_X, test_X, 1)
#        tr_X_2, test_X_2 = feature_sample(tr_X, test_X, 9999)
#        tr_X_3, test_X_3 = feature_sample(tr_X, test_X, 9999)

	model_lr = LogisticRegression(solver='sag', max_iter=5000)
        clf_lr = model_lr.fit(tr_X, tr_y)
        predict_lr = clf_lr.predict(test_X)
        prob_lr = clf_lr.predict_proba(test_X)

	model_svc = SVC(probability=True)
        clf_svc = model_svc.fit(tr_X, tr_y)
        predict_svc = clf_svc.predict(test_X)
        prob_svc = clf_svc.predict_proba(test_X)

	#model_dt = DecisionTreeClassifier(criterion='entropy')
	model_rf = RandomForestClassifier(n_estimators=500, random_state=1)
        clf_rf = model_rf.fit(tr_X, tr_y)
        predict_rf = clf_rf.predict(test_X)
        prob_rf = clf_rf.predict_proba(test_X)

        print len(predict_lr)

	try:
		os.remove('log_supervised.txt')
	except Exception:
		pass

        label_cnt = 0
        prob_th = 0.75
	with open('log_supervised.txt', 'a') as outfile:
	    for n, i in enumerate(predict_lr):
                prob = []
                prob.append(max(prob_lr[n]))

                i2 = predict_svc[n]
                prob.append(max(prob_svc[n]))

                i3 = predict_rf[n]
                prob.append(max(prob_rf[n]))

                prob_aver = hmean(prob)

                #print i, i2, i3
                #print prob
                #print '#############'

                #if i == i2 == i3 and prob[0] > prob_th and prob[1] > prob_th and prob[2] > prob_th:
                if i == i2 == i3 and min(prob) > prob_th:
                    label_cnt +=1
		    outfile.write(str(int(i)) + ';true;' + str(prob_aver) + '\n')
                else:
		    outfile.write(str(int(i)) + ';false;' + str(prob_aver) + '\n')

        return label_cnt


#run_supervised_ml('vlc_train.csv', 'vlc_test.csv', 0)
#run_selflearning('vlc_train.csv', 'vlc_test.csv')
