import sys, os, re
import expressions.compiler as Exp
from pymongo import MongoClient

def checkSig(func_file, sig):
	m = re.search(r'(.*):', sig)
	fsig = m.group(1)

	with open(func_file) as f:
		if fsig in f.read():
			return True

	return False

def show_path_classification(collection_name, project_name, round):

	client = MongoClient()
	db = client.errspec
	table = db[collection_name]

	res_file = project_name + '_' + str(round) + '.cres'
	func_file = project_name + '.lst'

	try:
		os.remove(res_file)
	except Exception:
		pass

	for i in table.find({'Determined': 0}):
		if i['SingleRet'] == 1:
			continue

		if checkSig(func_file, i['sig']) == False:
			continue

		with open(res_file, 'a') as of:
			of.write('Undetermined: ' + str(i['sig']) + ',' + str(i['returnVar']) + ',' + str(i['ClassificationRes']) + '\n')

	with open(res_file, 'a') as of:
		of.write('\n######################\n')

	for i in table.find({'Determined': 1}):
		if i['SingleRet'] == 1:
			continue

		if checkSig(func_file, i['sig']) == False:
			continue

		with open(res_file, 'a') as of:
			of.write('Determined: ' + str(i['sig']) + ',' + str(i['returnVar']) + ',' + str(i['ClassificationRes']) + '\n')

def eval_constant(var, code_base):
	if var == 'NULL' or var == 'false' or var == 'FALSE':
		return 2

	if var == 'true' or var == 'TRUE':
		return 1

        try:
	    value = Exp.EvaluateVar(var, code_base)
        except Exception:
            return 3

	if value == None:
	#parsing error.....
		return 3
	else:
		if value == 0:
			return 0
		elif value > 0:
			return 1
		else:
			return 2

def determine_range(r, v, code_base):
	if not r:
		return [str(v)]

	if r[0] not in ['>', '<', '!']:
		res = eval_constant(r, code_base)
		if res == 3:
			return [str(v)]
		else:
			return [str(res)]
	else:
		if v != 3:
			return [str(v)]

		if r[0] == '!':
			return [str(v)]

		m = re.search(r'(.*) (.*)', r)
		op = m.group(1)
		tv = eval_constant(m.group(2), code_base)

		if tv == 3:
			return [str(v)]

		if tv == 0:
			if op == '>':
				return ['1']
			if op == '<':
				return ['2']
			if op == '>=':
				return ['0', '1']
			if op == '<=':
				return ['0', '2']

		if tv == 1:
			if op == '>' or op == '>=':
				return ['1']
			if op == '<' or op == '<=':
				return ['0', '1', '2']

		if tv == 2:
			if op == '<' or op == '<=':
				return ['2']
			if op == '>' or op == '>=':
				return ['0', '1', '2']


def detect_bugs(collection_name, project_name, code_base):
	client = MongoClient()
	db = client.errspec
	table = db[collection_name]

	func_file = project_name + '.lst'

	with open(func_file) as f:
		for line in f:
			line = str(line).strip('\n')

			ret_var = dict()
			ret_value = dict()
			
			res = table.find({'sig': {'$regex': line}})
			if res.count() <= 1:
				continue

			for i in res:
				if i['SingleRet'] == 1:
					continue

				r_var = str(i['returnVar'][0])
				r_value = determine_range(i['returnRange'][0], i['Retrelationtype'], code_base)

				if not ret_var:
					ret_var[i['ClassificationRes']] = [r_var]
				else:
					if i['ClassificationRes'] in ret_var:
						ret_var[i['ClassificationRes']].append(r_var)
					else:
						ret_var[i['ClassificationRes']] = [r_var]

				if not ret_value:
					ret_value[i['ClassificationRes']] = r_value
				else:
					if i['ClassificationRes'] in ret_value:
						ret_value[i['ClassificationRes']] += r_value
					else:
						ret_value[i['ClassificationRes']] = r_value

			var_lst = []
			for key in ret_var:
				var_lst.append(set(ret_var[key]))

			value_lst = []
			for key in ret_value:
				value_lst.append(set(ret_value[key]))

			if not var_lst or len(var_lst) == 1:
				continue

			var_conflict = set.intersection(*var_lst)
			value_conflict = set.intersection(*value_lst)
			if value_conflict == set(['3']):
				value_conflict = None

			if var_conflict or value_conflict:
				print "######POTENTIAL BUGS IN FUNC: " + line

				print "****return var*****"
				print var_lst

				print "****return value*****"
				print value_lst

				print '=============================='




#detect_bugs('httpd', 'httpd', 'httpd-2.4.29')





